.DEFAULT_GOAL:=help

# --------------------------
include .env
export
# --------------------------

.PHONY: build up

build: #📦 Build docker image of fiatwise application
	DOCKER_BUILDKIT=1 docker build -t fiatwise .

up: #🚀 Deploy containers
	docker-compose up -d

down: #🚀 turn off containers
	docker-compose down

help:
	@echo "Fiatwise Backend Task"
	@echo "---------------------"
	@echo "Follow the next steps:"
	@echo "make build 📦"
	@echo "make up 🚀"
	@echo "---------------------"
