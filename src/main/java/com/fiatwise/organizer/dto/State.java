package com.fiatwise.organizer.dto;

public enum State {
    SUBSCRIBED, LEFT, COMPLETED;
}
