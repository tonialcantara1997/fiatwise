package com.fiatwise.organizer.exception;

public class UniversityNotExistsException extends Exception {

    public UniversityNotExistsException() {
        super("University not found");
    }
}
