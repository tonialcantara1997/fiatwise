package com.fiatwise.organizer.mapper;

import com.fiatwise.organizer.dto.StudentModifierDto;
import com.fiatwise.organizer.dto.StudentSearchResultDto;
import com.fiatwise.organizer.dto.StudentUniversityHistoryDto;
import com.fiatwise.organizer.dto.StudentWriterDto;
import com.fiatwise.organizer.model.Student;
import com.fiatwise.organizer.model.StudentUniversityState;
import org.mapstruct.*;

import java.util.Comparator;
import java.util.List;

@Mapper(componentModel = "spring", uses = StudentUniversityStateMapper.class)
public interface StudentMapper {

    Student map(StudentWriterDto swd);

    StudentSearchResultDto map(Student student);
    List<StudentSearchResultDto> map(List<Student> student);

    @BeanMapping(
            nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
            nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS
    )
    Student updateStudentFromDto(StudentModifierDto dto, @MappingTarget Student student);

    @AfterMapping
    default void getStateAndUniversity(Student student, @MappingTarget StudentSearchResultDto ssrd) {
        var sus = student.getStudentUniversityStateList();
        if (sus != null && !sus.isEmpty()) {
            // Prioritize States with null end date
            var currentSusNullEndDate = sus.parallelStream()
                                                    .filter(value -> value.getEndDate() == null)
                                                    .max(
                                                            Comparator.comparing(
                                                                    StudentUniversityState::getCreationDate
                                                            )
                                                    );

            if (currentSusNullEndDate.isPresent()) {
                ssrd.setState(currentSusNullEndDate.get().getState());
                var university = currentSusNullEndDate.get().getUniversity();
                ssrd.setUniversityName(
                        university != null ? university.getName() : ""
                );
            } else {
                var currentSus = sus.parallelStream()
                        .filter(value -> value.getEndDate() != null)
                        .max(
                                Comparator.comparing(
                                        StudentUniversityState::getCreationDate
                                )
                        ).orElseThrow();

                ssrd.setState(currentSus.getState());

                var university = currentSus.getUniversity();
                ssrd.setUniversityName(
                        university != null ? university.getName() : ""
                );
            }
        }
    }

    @Mapping(target = "universityHistoryDtoList", source = "student.studentUniversityStateList")
    StudentUniversityHistoryDto entityToSuhDto(Student student);
}
