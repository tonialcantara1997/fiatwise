package com.fiatwise.organizer.mapper;

import com.fiatwise.organizer.dto.UniversityHistoryDto;
import com.fiatwise.organizer.model.StudentUniversityState;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring")
public interface StudentUniversityStateMapper {

    @Mapping(target = "universityName", source = "sus.university.name")
    UniversityHistoryDto map(StudentUniversityState sus);
    List<UniversityHistoryDto> map(List<StudentUniversityState> susList);
}
