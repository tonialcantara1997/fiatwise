package com.fiatwise.organizer.mapper;

import com.fiatwise.organizer.dto.UniversityModifierDto;
import com.fiatwise.organizer.dto.UniversitySearchResultDto;
import com.fiatwise.organizer.dto.UniversityWriterDto;
import com.fiatwise.organizer.model.University;
import org.mapstruct.*;

@Mapper(componentModel = "spring")
public interface UniversityMapper {

    University map(UniversityWriterDto uwd);

    UniversitySearchResultDto map(University university);

    @BeanMapping(
            nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
            nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS
    )
    University updateUniversityFromDto(UniversityModifierDto dto, @MappingTarget University university);
}
